// Setup Gulp Vars
var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync').create();
var imagemin = require('gulp-imagemin');
var bourbon = require('node-bourbon');

// Bourbon paths
bourbon.with('src/scss/*.scss');

// Tasks

// Sass
gulp.task('sass', function(){
	return gulp.src('src/scss/*.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(sass({
		includePaths: require('node-bourbon').includePaths,
		style: 'compressed',
		quiet: true
	}))
	.pipe(gulp.dest(''))
	.pipe(browserSync.stream())
});

// Uglify
gulp.task('uglify', function(){
	return gulp.src('src/js/*.js')
	.pipe(uglify())
	.pipe(gulp.dest('js'))
	.pipe(browserSync.stream())
});
// Watch

gulp.task('default', function(){
	browserSync.init({
		proxy: 'crc.dev'
	});
	gulp.watch('*.php').on('change', browserSync.reload);
	gulp.watch('src/scss/*.scss', ['sass']);
	gulp.watch('src/js/*.js', ['uglify']);
});

// Imagemin
gulp.task('imagemin', function(){
	return gulp.src('src/images/*')
	.pipe(imagemin({progressive: true}))
	.pipe(gulp.dest('images'))
});